import React from "react";

const Update = props => {
  let status = "";
  if (props.updated) {
    status = "Draft saved successfully!";
  }
  if (props.updateError) {
    status = "There was an error saving your draft. Please try again!";
  }
  return <div>{status}</div>;
};

export default Update;
