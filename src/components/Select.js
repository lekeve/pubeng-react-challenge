import React from "react";

const createRange = (lower, upper) => {
  let options = [<option key="default" />];
  for (let i = lower; i <= upper; i++) {
    options.push(
      <option key={i} value={i}>
        {i}
      </option>
    );
  }
  return options;
};

export default props => {
  return (
    <select {...props} value={props.value || ""} required>
      {createRange(...props.range)}
    </select>
  );
};
