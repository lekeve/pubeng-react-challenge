import React from "react";
import {
  Checkbox,
  Repeatable,
  Text,
  Textarea,
  Select,
  Loading,
  Update
} from "./components";
import api from "./mockApi";

/*
  A note regarding the changes in my first commit:
  The majority of changes in this commit are syntax and formatting updates from Prettier. This includes semicolons at the end of statements, double quotes instead of single quotes, removing unneeded parentheses, etc.
  Normally I would consider reverting the updates made for the sake of completing the challenge but in a codebase I was working on full time I would consider pushing for these formatting updates, so I left them in. ( Of course, not in an overly pushy way but I enjoy lively discussions about JavaScript best practices and formatting. I actually like semicolons!)
*/

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      updated: false,
      updateError: false,
      data: {
        title: "",
        rating: 0,
        year: null,
        description: "",
        upcoming: false,
        cast: []
      }
    };

    // CHANGED
    // Arrow functions over binding `this` in the constructor. Personal preference for readability.

    // this.handleChange = this.handleChange.bind(this);
    // this.handleUpdate = this.handleUpdate.bind(this);
    // this.Input = this.Input.bind(this);
  }

  componentDidMount() {
    if (this.props.id) {
      this.setState({ loading: true });
      api
        .get(this.props.id)
        .then(data => {
          this.setState({ loading: false, data: data });
        })
        .catch(data => {
          console.log("Movie not found in database.");
          this.setState({ loading: false });
        });
    }
  }

  /**
   * @desc Handles change to form Inputs
   * @param {Object} delta Updates made to Input child
   * @return undefined
   */
  handleChange = delta => {
    // update state with delta
    this.setState(({ data }) => ({ data: { ...data, ...delta } }));
  };

  /**
   * @desc Handles updates to draft on form Input blur
   * @param {Boolean} publish Default false, true if publish button clicked
   * @return {Object} Draft form data
   */
  handleUpdate = async (publish = false) => {
    this.setState({ loading: true, updated: false, updateError: false });
    // destructure state to unpack data
    const { data } = this.state;
    // post data and store in results
    const results = await api
      .post({ ...data, publish })
      .then(data => {
        this.setState({ loading: false, updated: true });
      })
      .catch(data => {
        this.setState({ loading: false, updated: false, updateError: true });
      });
    // print feedback after getting results
    console.log("Content updated!");
    // return results form data object
    return results;
  };

  /**
   * @desc Input component
   * @param {Function} children Function as child pattern
   * @param {Boolean} iterable Default undefined, true if repeatable
   * @param {String} label Element label
   * @param {String} id Element id
   * @param {Boolean} integer Default undefined, true if Input value needs to be formatted
   */
  Input = ({ children, iterable, label, id, integer }) => {
    // CHANGED
    // Added an integer prop to detect if target value needed to be converted into an integer
    // This was done because I reused the Select component for ratings.
    // This change could have been avoided by refactoring the Text component to have users enter a film rating but I like the idea of having users select a rating from a selection menu like IMDB has implemented

    const handleChange = value => {
      this.handleChange({ [id]: value });
    };
    const value = this.state.data[id];
    let props = {};

    if (iterable) {
      props = {
        id,
        value,
        onCreate: item =>
          handleChange([
            ...value,
            {
              ...item,
              id: Math.floor(Math.random() * 100000)
            }
          ]),
        onUpdate: item =>
          handleChange(
            value.map(prev => {
              if (item.id === prev.id) {
                return item;
              }
              return prev;
            })
          ),
        onDelete: id => handleChange(value.filter(prev => prev.id !== id))
      };
    } else {
      props = {
        id,
        value,
        onBlur: () => this.handleUpdate(false),
        // CHANGED Update regarding the integer param
        onChange: e => {
          if (e.target.type === "checkbox") {
            handleChange(e.target.checked);
          } else {
            integer
              ? handleChange(parseInt(e.target.value, 10))
              : handleChange(e.target.value);
          }
        }
      };
    }
    return (
      <div className="Form-Group">
        <div className="Form-Label">{label}</div>
        {children(props)}
      </div>
    );
  };

  render() {
    const { Input } = this;
    return (
      <div className="Form">
        {this.state.loading ? <Loading /> : null}
        <Update
          updated={this.state.updated}
          updateError={this.state.updateError}
        />
        <Input label="Title" id="title">
          {props => <Text {...props} />}
        </Input>
        <Input label="Upcoming" id="upcoming">
          {props => <Checkbox {...props} />}
        </Input>
        <Input label="Year" id="year" integer>
          {props => <Select {...props} range={[2010, 2020]} />}
        </Input>
        <Input label="Rating" id="rating" integer>
          {props => <Select {...props} range={[1, 10]} />}
        </Input>
        <Input label="Description" id="description">
          {props => <Textarea {...props} />}
        </Input>
        <Input label="Cast" iterable id="cast">
          {props => <Repeatable {...props} />}
        </Input>
        <button onClick={() => this.handleUpdate(true)}>{"Publish"}</button>
      </div>
    );
  }
}

export default App;
